package com.cassone.devmalin.mydrink.Activity;

/* Classe : MapActivity.java
   Description : Activity permettant d'afficher les bars sur une map autour de la position de
   l'utilisateur, localiser un bar pr�vis ou afficher les bars d'un groupe d'ami.
   Auteur : Julien CASSONE
 */

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cassone.devmalin.mydrink.Controller.JsonParserAPI;
import com.cassone.devmalin.mydrink.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("ConstantConditions")
public class MapsActivity extends AppCompatActivity implements LocationListener, GoogleMap.OnInfoWindowClickListener, View.OnClickListener, OnMapReadyCallback {

    protected ImageButton imageButtonHome;

    protected TextView textViewMapTitle;

    private LocationManager locationManager;
    private Marker marker;
    protected Marker markerBar;

    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    Location location;
    double latitude;
    double longitude;

    private int idUser;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
    private static final long MIN_TIME_BW_UPDATES = 1000;
    private static final long MIN_ZOOM = 14;
    private static final long MIN_ZOOM_FOCUS = 10;

    LatLng latLng;

    private String mode;
    protected String nameBarFocused;
    protected String adressBarFocused;
    protected String cityBarFocused;
    protected String countryBarFocused;
    protected double latitudeBarFocused;
    protected double longitudeBarFocused;

    protected int groupId;
    protected String groupName;
    protected String groupStyle;
    protected String groupBar;
    protected String groupHour;
    protected String groupHH;
    protected String groupLastBar;

    JsonParserAPI jsonParserAPI = new JsonParserAPI();

    protected static String url_get_bar = "http://167.114.228.209/get-bar-prox.php";
    protected String url_get_group = "http://167.114.228.209/get-group.php";
    protected String url_get_member = "http://167.114.228.209/get-member.php";
    protected String url_get_bar_group = "http://167.114.228.209/get-bar-group.php";
    protected String url_set_location = "http://167.114.228.209/set-location.php";

    private static String TAG_SUCCESS = "success";

    private static int success;

    private static ArrayList<HashMap<String, String>> arrayListBar = null;

    protected ArrayList<Marker> listMarkerBar = null;

    protected SharedPreferences preferences;

    private ArrayList<String> arrayListLat;
    private ArrayList<String> arrayListLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colortheme)));
        getSupportActionBar().setCustomView(R.layout.action_bar_layout_second);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_maps);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        imageButtonHome = (ImageButton) findViewById(R.id.imageButtonHomeProfil);
        imageButtonHome.setOnClickListener(this);

        textViewMapTitle = (TextView) findViewById(R.id.textViewSocialProfilPseudo);

        listMarkerBar = new ArrayList<>();

        arrayListBar = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        preferences = getSharedPreferences("CONNECTION", Context.MODE_PRIVATE);
        mode = preferences.getString("MODE", null);
        idUser = preferences.getInt("ID", -1);
        // Les actions vont �tre diff�rente selon l'activit� qui fut pr�c�d�e
        switch (mode) {
            case "single": {
                textViewMapTitle.setText(R.string.title_map_solo);
                break;
            }
            case "focus": {
                // R�cup�ration des donn�es du bar � localiser
                nameBarFocused = preferences.getString("BARNAME", null);
                adressBarFocused = preferences.getString("BARADRESS", null);
                cityBarFocused = preferences.getString("BARCITY", null);
                countryBarFocused = preferences.getString("BARCOUNTRY", null);
                latitudeBarFocused = Double.parseDouble(preferences.getString("LATITUDE", null));
                longitudeBarFocused = Double.parseDouble(preferences.getString("LONGITUDE", null));

                textViewMapTitle.setText(nameBarFocused);

                break;
            }
            case "group":
                // R�cup�ration du groupe
                groupId = getIntent().getIntExtra("GROUPID", -1);

                textViewMapTitle.setText(R.string.title_map_group);

                break;
        }

        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        // Connexion � la g�olocalisation via GPS ou network
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_no_gps_connection), Toast.LENGTH_LONG).show();
        } else {
            canGetLocation = true;
            if (isNetworkEnabled) {
                abonnementNETWORK();
                if (locationManager != null) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                }
            }

            if (isGPSEnabled) {
                if (location == null) {
                    abonnementGPS();
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }

            latLng = new LatLng(latitude, longitude);
            marker.setPosition(latLng);
        }
    }

    @Override
    public void onBackPressed() {

        finish();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("MODE", "single");
        editor.commit();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPause() {
        super.onPause();

        //On appelle la m�thode pour se d�sabonner
        desabonnementGPS();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        //Mise � jour des coordonn�es
        latLng = new LatLng(latitude, longitude);
        marker.setPosition(latLng);

        if (mode.equals("group"))
            new SetLocation().execute();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
        //Si le GPS est activ? on s'abonne
        if ("gps".equals(provider)) {
            abonnementGPS();
        } else if ("network".equals(provider)) {
            abonnementNETWORK();
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Si le GPS est d?sactiv? on se d?sabonne
        if ("gps".equals(provider)) {
            desabonnementGPS();
        } else if ("network".equals(provider)) {
            desabonnementNETWORK();
        }
    }

    /* Fonction qui permet de s'abonner au GPS via internet */
    public void abonnementGPS() {
        //On s'abonne
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
    }

    /* Fonction qui permet de d�sactiv� le GPS si l'utilisateur quitte la page */
    public void desabonnementGPS() {
        //Si le GPS est disponible, on s'y abonne
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(this);
    }

    /* Fonction qui permet de s'abonner au GPS via internet */
    public void abonnementNETWORK() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
    }

    /* Fonction qui permet de d�sactiv� le GPS via internet si l'utilisateur quitte la page */
    public void desabonnementNETWORK() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButtonHomeProfil:
                Intent intentHome = new Intent(this, MainActivity.class);
                startActivity(intentHome);
                finish();
                break;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (marker.getTitle().compareTo(getResources().getString(R.string.toast_map_position)) != 0 && !mode.equals("focus") && marker.getSnippet() != null) {
            Intent intentBar = new Intent(getApplicationContext(), DetailBarActivity.class);
            intentBar.putExtra("bar", arrayListBar.get(listMarkerBar.indexOf(marker)));
            startActivity(intentBar);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setOnInfoWindowClickListener(this);
        // Position de l'utilisateur
        marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Votre position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        if (mode.equals("single")) {
            // Affichage des dit bars les plus proche de l'utilisateur
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MIN_ZOOM));

            if (arrayListBar.size() <= 0) {
                googleMap.clear();
                // Position de l'utilisateur
                marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Votre position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                new GetBar().execute(googleMap);
            }
        } else if (mode.equals("focus")) {
            // Affichage du bar s�lectionn�
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MIN_ZOOM_FOCUS));
            markerBar = googleMap.addMarker(new MarkerOptions().position(new LatLng(latitudeBarFocused, longitudeBarFocused)).title(nameBarFocused).snippet(adressBarFocused));
        }

        if (mode.equals("group")) {
            // Affichage des bars pr�t du groupe
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MIN_ZOOM));

            new GetGroup().execute();
        }
    }

    /* WebService qui r�cup�re les bars autour de l'utilisateur en mode "single" */
    class GetBar extends AsyncTask<GoogleMap, GoogleMap, GoogleMap> {

        private String response;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        HashMap<String,String> mapBar;

        JSONObject e;

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
        }

        @Override
        protected GoogleMap doInBackground(GoogleMap... mMap) {

            HashMap<String, String> params = new HashMap<>();
            params.put("latitude", String.valueOf(latitude));
            params.put("longitude", String.valueOf(longitude));

            Object json = null;
            try {
                json = jsonParserAPI.makeHttpRequest(url_get_bar, "POST", params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (json instanceof JSONObject) {
                jsonObject = (JSONObject) json;
                Log.d("Login User", json.toString());
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    response = jsonObject.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (json instanceof JSONArray) {
                success = 0;
                jsonArray = (JSONArray) json;
            }
            else if (json == null) {
                success = -1;
                Log.d("Login User", "Erreur connexion");
            }

            return null;
        }

        @Override
        protected void onPostExecute(GoogleMap mMap) {
            // dismiss the dialog once done
            if(success == 1) {
            }
            else if (success == 0){
                arrayListBar = new ArrayList<>();
                listMarkerBar = new ArrayList<>();

                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mapBar = new HashMap<>();
                        e = jsonArray.getJSONObject(i);

                        // R�cup�ration des donn�es des bars
                        mapBar.put("id", e.getString("id_bar"));
                        mapBar.put("name", e.getString("name_bar"));
                        mapBar.put("phone", e.getString("phone_bar"));
                        mapBar.put("site", e.getString("site_bar"));
                        mapBar.put("adress", e.getString("adress_bar"));
                        mapBar.put("country", e.getString("country_bar"));
                        mapBar.put("cp", e.getString("cp_bar"));
                        mapBar.put("city", e.getString("city_bar"));
                        mapBar.put("style", e.getString("style_bar"));
                        mapBar.put("comment", e.getString("comment_bar"));
                        mapBar.put("allomatch", e.getString("allomatch_bar"));
                        mapBar.put("price_beer", e.getString("price_beer_bar"));
                        mapBar.put("price_beer_hh", e.getString("price_beer_bar_HH"));
                        mapBar.put("price_wine", e.getString("price_wine_bar"));
                        mapBar.put("price_wine_hh", e.getString("price_wine_bar_HH"));
                        mapBar.put("price_cocktail", e.getString("price_cocktail_bar"));
                        mapBar.put("price_cocktail_hh", e.getString("price_cocktail_bar_HH"));
                        mapBar.put("open_monday", e.getString("open_monday_bar"));
                        mapBar.put("close_monday", e.getString("close_monday_bar"));
                        mapBar.put("open_tuesday", e.getString("open_tuesday_bar"));
                        mapBar.put("close_tuesday", e.getString("close_tuesday_bar"));
                        mapBar.put("open_wednesday", e.getString("open_wednesday_bar"));
                        mapBar.put("close_wednesday", e.getString("close_wednesday_bar"));
                        mapBar.put("open_thuresday", e.getString("open_thuresday_bar"));
                        mapBar.put("close_thuresday", e.getString("close_thuresday_bar"));
                        mapBar.put("open_friday", e.getString("open_friday_bar"));
                        mapBar.put("close_friday", e.getString("close_friday_bar"));
                        mapBar.put("open_saturday", e.getString("open_saturday_bar"));
                        mapBar.put("close_saturday", e.getString("close_saturday_bar"));
                        mapBar.put("open_sunday", e.getString("open_sunday_bar"));
                        mapBar.put("close_sunday", e.getString("close_sunday_bar"));
                        mapBar.put("open_hh", e.getString("open_hh_bar"));
                        mapBar.put("close_hh", e.getString("close_hh_bar"));
                        mapBar.put("latitude", e.getString("latitude_bar"));
                        mapBar.put("longitude", e.getString("longitude_bar"));
                        mapBar.put("googlemap_id_bar", e.getString("googlemap_id_bar"));

                        arrayListBar.add(mapBar);
                        markerBar = mMap.addMarker(new MarkerOptions().position(new LatLng(e.getDouble("latitude_bar"), e.getDouble("longitude_bar"))).title(arrayListBar.get(i).get("name")).snippet(arrayListBar.get(i).get("adress")));
                        listMarkerBar.add(markerBar);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (success == -1) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_no_internet_connection), Toast.LENGTH_LONG).show();
            }
        }
    }

    /* WebService qui r�cup�re les informations sur le groupe */
    class GetGroup extends AsyncTask<String, String, String> {

        int success;
        private String response;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {

            HashMap<String, String> params = new HashMap<>();
            params.put("idgroup", String.valueOf(groupId));

            Object json = null;
            try {
                json = jsonParserAPI.makeHttpRequest(url_get_group, "POST", params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (json instanceof JSONObject) {
                jsonObject = (JSONObject) json;
                Log.d("Login User", json.toString());
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    response = jsonObject.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (json instanceof JSONArray) {
                success = 0;
                jsonArray = (JSONArray) json;
            }
            else if (json == null) {
                success = -1;
                Log.d("Login User", "Erreur connexion");
            }

            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            if(success == 0) {

                try {
                    JSONObject e = jsonObject;

                    // R�cup�ration des donn�es du groupe
                    groupName = e.getString("name_group");
                    groupStyle = e.getString("style_group");
                    groupHour = e.getString("hour_group");
                    groupHH = e.getString("happy_hour_group");
                    groupBar = e.getString("bar_group");
                    groupLastBar = e.getString("last_bar_group");

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                new GetMember().execute();
            }
            else if (success == 1) {
            }
            else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_no_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    /* WebSerice qui r�cup�re les members du groupe et leur position sur la map */
    class GetMember extends AsyncTask<GoogleMap, String, GoogleMap> {

        private String response;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        int success;

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
        }

        @Override
        protected GoogleMap doInBackground(GoogleMap... mMap) {

            HashMap<String, String> params = new HashMap<>();
            params.put("idgroup", String.valueOf(groupId));

            Object json = null;
            try {
                json = jsonParserAPI.makeHttpRequest(url_get_member, "POST", params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (json instanceof JSONObject) {
                jsonObject = (JSONObject) json;
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    response = jsonObject.getString("message");
                    Log.d("Login User", json.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (json instanceof JSONArray) {
                success = 0;
                jsonArray = (JSONArray) json;
            }
            else if (json == null) {
                success = -1;
                Log.d("Login User", "Erreur connexion");
            }

            return null;
        }

        @Override
        protected void onPostExecute(GoogleMap mMap) {
            // dismiss the dialog once done
            if(success == 0) {

                try {
                    mMap.clear();
                    // Position de l'utilisateur
                    marker = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Votre position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

                    arrayListLat = new ArrayList<>();
                    arrayListLng = new ArrayList<>();
                    arrayListBar = new ArrayList<>();

                    arrayListLat.add(String.valueOf(latitude));
                    arrayListLng.add(String.valueOf(longitude));

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject e = jsonArray.getJSONObject(i);
                        if (!e.getString("latitude_user").equals("null") && !e.getString("longitude_user").equals("null") && e.getInt("id_user") != idUser) {
                            // Marker des membres
                            mMap.addMarker(new MarkerOptions().position(new LatLng(e.getDouble("latitude_user"), e.getDouble("longitude_user"))).title(e.getString("nickname_user")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
                            arrayListLat.add(String.valueOf(e.getDouble("latitude_user")));
                            arrayListLng.add(String.valueOf(e.getDouble("longitude_user")));
                        }
                    }

                    if (arrayListBar.size() <= 0)
                        new GetBarGroup().execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (success == 1) {
            }
            else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_no_internet_connection), Toast.LENGTH_LONG).show();
            }
        }
    }

    /* WebService qui envoie la position de l'utilisateur au serveur */
    class SetLocation extends AsyncTask<String, String, String> {

        int success;
        private String response;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... args) {

            HashMap<String, String> params = new HashMap<>();
            params.put("iduser", String.valueOf(idUser));
            params.put("latitude", String.valueOf(latitude));
            params.put("longitude", String.valueOf(longitude));

            Object json = null;
            try {
                json = jsonParserAPI.makeHttpRequest(url_set_location , "POST", params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (json instanceof JSONObject) {
                jsonObject = (JSONObject) json;
                Log.d("Login User", json.toString());
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    response = jsonObject.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (json instanceof JSONArray) {
                success = 0;
                jsonArray = (JSONArray) json;
            }
            else if (json == null) {
                success = -1;
                Log.d("Login bar", "Erreur connexion");
            }

            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            if(success == 0) {
                new GetGroup().execute();
            }
            else if (success == 1) {
            }
            else
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_no_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    /* WebServie r�cup�rant les bars par rapport au param�tre et � la triangulation du groupe */
    class GetBarGroup extends AsyncTask<GoogleMap, String, GoogleMap> {

        private String response;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        HashMap<String,String> mapBar;

        JSONObject e;

        @Override
        protected void onPreExecute () {
            super.onPreExecute();
        }

        @Override
        protected GoogleMap doInBackground(GoogleMap... mMap) {

            String listLat = "";
            String listLng = "";

            int sizePos = arrayListLat.size();

            // Construction du contenaire des identifiant des utilisateurs
            for (int i = 0; i < sizePos; i++) {
                listLat += arrayListLat.get(i);
                listLng += arrayListLng.get(i);

                if(i != sizePos -1) {
                    listLat += " ";
                    listLng += " ";
                }
            }

            HashMap<String, String> params = new HashMap<>();
            params.put("style", groupStyle);
            params.put("hour", groupHour);
            params.put("happyhour", groupHH);
            params.put("latitude", listLat);
            params.put("longitude", listLng);

            Object json = null;
            try {
                json = jsonParserAPI.makeHttpRequest(url_get_bar_group, "POST", params);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (json instanceof JSONObject) {
                jsonObject = (JSONObject) json;
                Log.d("Login User", json.toString());
                try {
                    success = jsonObject.getInt(TAG_SUCCESS);
                    response = jsonObject.getString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (json instanceof JSONArray) {
                success = 0;
                jsonArray = (JSONArray) json;
            }
            else if (json == null) {
                success = -1;
                Log.d("Login User", "Erreur connexion");
            }

            return null;
        }

        @Override
        protected void onPostExecute(GoogleMap mMap) {
            // dismiss the dialog once done
            if(success == 1) {
            }
            else if (success == 0){
                arrayListBar = new ArrayList<>();
                listMarkerBar = new ArrayList<>();

                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        mapBar = new HashMap<>();
                        e = jsonArray.getJSONObject(i);

                        // R�cup�ration des donn�es des bars
                        mapBar.put("id", e.getString("id_bar"));
                        mapBar.put("name", e.getString("name_bar"));
                        mapBar.put("phone", e.getString("phone_bar"));
                        mapBar.put("site", e.getString("site_bar"));
                        mapBar.put("adress", e.getString("adress_bar"));
                        mapBar.put("country", e.getString("country_bar"));
                        mapBar.put("cp", e.getString("cp_bar"));
                        mapBar.put("city", e.getString("city_bar"));
                        mapBar.put("style", e.getString("style_bar"));
                        mapBar.put("comment", e.getString("comment_bar"));
                        mapBar.put("allomatch", e.getString("allomatch_bar"));
                        mapBar.put("price_beer", e.getString("price_beer_bar"));
                        mapBar.put("price_beer_hh", e.getString("price_beer_bar_HH"));
                        mapBar.put("price_wine", e.getString("price_wine_bar"));
                        mapBar.put("price_wine_hh", e.getString("price_wine_bar_HH"));
                        mapBar.put("price_cocktail", e.getString("price_cocktail_bar"));
                        mapBar.put("price_cocktail_hh", e.getString("price_cocktail_bar_HH"));
                        mapBar.put("open_monday", e.getString("open_monday_bar"));
                        mapBar.put("close_monday", e.getString("close_monday_bar"));
                        mapBar.put("open_tuesday", e.getString("open_tuesday_bar"));
                        mapBar.put("close_tuesday", e.getString("close_tuesday_bar"));
                        mapBar.put("open_wednesday", e.getString("open_wednesday_bar"));
                        mapBar.put("close_wednesday", e.getString("close_wednesday_bar"));
                        mapBar.put("open_thuresday", e.getString("open_thuresday_bar"));
                        mapBar.put("close_thuresday", e.getString("close_thuresday_bar"));
                        mapBar.put("open_friday", e.getString("open_friday_bar"));
                        mapBar.put("close_friday", e.getString("close_friday_bar"));
                        mapBar.put("open_saturday", e.getString("open_saturday_bar"));
                        mapBar.put("close_saturday", e.getString("close_saturday_bar"));
                        mapBar.put("open_sunday", e.getString("open_sunday_bar"));
                        mapBar.put("close_sunday", e.getString("close_sunday_bar"));
                        mapBar.put("open_hh", e.getString("open_hh_bar"));
                        mapBar.put("close_hh", e.getString("close_hh_bar"));
                        mapBar.put("latitude", e.getString("latitude_bar"));
                        mapBar.put("longitude", e.getString("longitude_bar"));
                        mapBar.put("googlemap_id_bar", e.getString("googlemap_id_bar"));

                        arrayListBar.add(mapBar);

                        markerBar = mMap.addMarker(new MarkerOptions().position(new LatLng(e.getDouble("latitude_bar"), e.getDouble("longitude_bar"))).title(arrayListBar.get(i).get("name")).snippet(arrayListBar.get(i).get("adress")));
                        listMarkerBar.add(markerBar);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (success == -1) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toast_no_internet_connection), Toast.LENGTH_LONG).show();
            }
        }
    }
}

