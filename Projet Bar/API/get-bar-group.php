<?php
	if (isset($_POST['style']) && 
		isset($_POST['hour']) && 
		isset($_POST['happyhour']) && 
		isset($_POST['latitude']) && 
		isset($_POST['longitude']))
	{
		$response = array();
		$style = $_POST['style'];
		$hour = $_POST['hour'];
		$happyhour = $_POST['happyhour'];
		$listlat = explode(" ", $_POST['latitude']);
		$listlng = explode(" ", $_POST['longitude']);

		require_once __DIR__ . '/api.php';

		$db = new API();
		
		mysql_query("SET NAMES UTF8");

		$count = 0;
		$sumlat = 0;
		$sumlng = 0;
		foreach($listlat as $idlat)
		{
			$sumlat += $idlat;
			$count++;
		}

		foreach($listlng as $idlng)
		{
			$sumlng += $idlng;
		}

		$moylat = $sumlat / $count;
		$moylng = $sumlng / $count;
	
		$geocode = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $moylat . ',' . $moylng .'&sensor=false');

		$output = json_decode($geocode);

		for($j = 0 ; $j < count($output->results[0]->address_components) ; $j++)
		{
               		$cn = array($output->results[0]->address_components[$j]->types[0]);
           		if(in_array("country", $cn))
           		{
            			$country = $output->results[0]->address_components[$j]->long_name;
           		}
			else if(in_array("locality", $cn))
			{
				$city = $output->results[0]->address_components[$j]->long_name;
			}
            	}

		$getbarposnull = mysql_query("SELECT * FROM bar WHERE country_bar = '$country' AND city_bar = '$city' AND (latitude_bar IS NULL OR longitude_bar IS NULL OR googlemap_id_bar IS NULL)");

		if (mysql_num_rows($getbarposnull) > 0)
		{
			while($line = mysql_fetch_array($getbarposnull))
			{
				$idbar = $line['id_bar'];
				$adressbar = $line['name_bar'] . ' ' . $line['cp_bar'] . ' ' . $city . ' ' . $country;
				$fulladress = str_replace(" ", "+", $adressbar);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$fulladress";

				$json = json_decode(file_get_contents($url),TRUE);

				$lat = $json['results'][0]['geometry']['location']['lat'];
				$lng = $json['results'][0]['geometry']['location']['lng'];
				$reference = $json['results'][0]['place_id'];

				if ($lat != "" && $lng != "")
				{
					$addposbar = mysql_query("UPDATE bar SET latitude_bar = $lat,longitude_bar = $lng,googlemap_id_bar = '$reference' WHERE id_bar = $idbar");
				}
			}
		}

		$countbar = 0;
		$raybar = 0.02;
		$count = 0;

		while ($countbar < 10 && $count < 1000) 
		{
			$latraymin = $moylat - $raybar;
			$latraymax = $moylat + $raybar;
			$lngraymin = $moylng - $raybar;
			$lngraymax = $moylng + $raybar;
			
			if ($style == "null")
			{
				$getbar = mysql_query("SELECT * FROM bar WHERE latitude_bar >= $latraymin AND latitude_bar <= $latraymax AND longitude_bar >= $lngraymin AND longitude_bar <= $lngraymax LIMIT 10");	
			}
			else
			{
				$getbar = mysql_query("SELECT * FROM bar WHERE style_bar = '$style' AND latitude_bar >= $latraymin AND latitude_bar <= $latraymax AND longitude_bar >= $lngraymin AND longitude_bar <= $lngraymax LIMIT 10");
				
			}

			$countbar = mysql_num_rows($getbar);
			$raybar *= 2;
			$count++;
		}

		while($line = mysql_fetch_array($getbar))
		{
			$response[] = $line;
		}

		echo json_encode($response);

	}
	else
	{
		$response["success"] = 5;
		$response["message"] = "Erreur";
		echo json_encode($response);
	}
?>
