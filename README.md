# README #

Version 1.0

Ce README a pour but de présenter les différentes démonstrations de mes codes.

A ce jour, je présente dans un premier temps une partie de mon code sur un projet de bar: 

C'est un projet professionnel que je voulais lancer sur le marché avec des marketings venant d'une école de commerce.
Le projet a duré un an et j'ai pu le rattacher à mon école pour en faire le projet annuel qui avait pour but de valider mon année de Master 2. Le projet dispose d'une version utilisable mais en raison de complication et de manque de moyens, l'application a du être retiré du Play store à cause de l'absence de serveur et le projet est donc en pause.

Cette application a pour but de trouver des bars a proximité d'une ou plusieurs personnes faisant partie d'un même groupe. Il est également possible de faire des recherches par filtre.

La partie du code que j'ai décidé de montrer est l’activité "MapsActivity.java". Cette Activity a 3 fonctions importantes selon l'information qui lui est transmise :

- Single : permet d'afficher les 10 bars les plus proches sur une map grâce à la géolocalisation de l'utilisateur
- Focus : permet de localisé un bar précis lorsque l'on vient de consulté les détails d'un bar précis
- Group : Affiche dans un premier temps la position via des markers des personnes faisant partie du même groupe et dans un deuxième temps, fait le calcul des positions de chaque utilisateur pour créer un point de centre pour afficher les bars proche de chaque utilisateur avec des critères de filtrage paramétrable.

 Les échanges de données se font via des APIs sur un serveur qui sont :

- get-bar-group.php : retourne les bars proches de chaque utilisateur du groupe (limité à 10)
- get-bar-prox.php : retourne les bars proche d'un utilisateur (limité à 10)
- get-group.php : retourne les informations du groupe sélectionné avant l'accès à la map
- get-member.php :retourne les membres faisant partis du groupe
- set-location : Met à jour la position géographique de l'utilisateur

La récupération des données du serveur sont possibles grâce à la classe JsonParserAPI.java. C'est une classe simple qui a pour but de parser les données venant des API. Je suis conscient que l'utilisation d'une librairie comme Jackson ou JSOUP aurait été plus pratique, mais comme je travaillais en équipe avec deux autres développeurs chargés de faire l'application sur IOS et Windows Phone, et étant le responsable et concepteur du projet, je voulais que le code soit facilement accessible sur les différentes plate-formes.

Les données des bars, des groupes et des membres sont stockés dans le serveur sur une base de données MySQL. Si ça ne tenait qu'à moi j'aurais utilisé des nouvelles technologies
comme MongoDB. Mais pour le confort de la majorité de l'équipe et par gain de temps, j'ai préféré opté pour une méthode un peu moins récente. De plus, plutôt que de stocker les données des bars dans la base de données, comme il me l'a été imposé, j'aurais préféré choisir des API déjà existante dans l'open data pour alléger le serveur.

Je vous souhaite donc une bonne lecture du code.